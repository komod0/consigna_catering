#language: es
Característica: Presupuestación de casamiento

Antecedentes:
  Dado que mi cuit personal es "30445556667"

  Escenario: c1 - Evento empresarial con menos de 20 personas
    Dado un evento "Evento empresarial" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 19 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error

  Escenario: c4 - Casamiento contratado por un empresa
    Dado un evento "Casamiento" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error

  Escenario: c5 - Fiesta de 15 contratada por una empresa
    Dado un evento "Fiesta de 15" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error

Antecedentes:
    Dado que mi cuit personal es "27389877762"

  Escenario: c6 - Casamiento con mas de 300 personas
    Dado un evento "Casamiento" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 301 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error

  Escenario: c7 - Casamiento con salon y mas de 150 personas
    Dado un evento "Casamiento" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que va a ser llevado a cabo en el salon provisto por el servicio de catering
    Y que tendrá 151 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error
