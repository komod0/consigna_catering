#language: es
Característica: Presupuestación de casamiento

Antecedentes:
  Dado que mi cuit personal es "30445556667"

  Escenario: v1 - Empresa no puede contratar casamiento
    Dado un evento "casamiento" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces obtengo un error